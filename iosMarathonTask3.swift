var numbers = [51, 733, 39, 245, 6, 13, 3569, 136, 164, 33, 9065, 314, 14, 1374]
let sortedUp = numbers.sorted(by: <) 
let sortedDown = numbers.sorted(by: >)

print("Написать сортировку массива с помощью замыкания, в одну сторону, затем в обратную.")
print("Вывести результат в консоль:")
print("По возрастанию: \(sortedUp)")
print("По убыванию: \(sortedDown)")

var names: [String] = []
func namesFunc(name: String) {
	names.append(name) 
}

namesFunc(name: "Vasiliy") 
namesFunc(name: "Petro") 
namesFunc(name: "Ilya")
namesFunc(name: "Alexey")

print("Создать метод, который принимает имена друзей, после этого имена положить в массив.")
print("Массив отсортировать по количеству букв в имени.")
print(names.sorted(by: {$0.count > $1.count}))

print("Создать словарь (Dictionary), где ключ - кол-во символов в имни, а в значении - имя друга.")
print("Написать функцию, которая будет принимать ключ, выводить полученный ключ и значение.")
var dict: [String : Int] = [:]

func putDict(key: String, value: Int) {
	print("Key: \(key) | Value: \(value)")
	dict[key] = value
}

for name in names {
	putDict(key: name, value: name.count)
}

var strings: [String] = []
var ints: [Int] = []
func empFunc(intArray: [Int], stringArray: [String]) {
	if strings.isEmpty { 
		strings.append("A") 
	}
	if ints.isEmpty { 
		ints.append(1)
	} 
	print(strings) 
	print(ints)
}


print("Написать функцию, которая принимает 2 массива (один строковый, второй - числовой) и проверяет их на пустоту: если пустой - то добавьте любое значения и выведите массив в консоль")
print("Тест 1")
empFunc(intArray: ints, stringArray: strings)
strings.append("qwerty")
strings.append("asdfg")
print("Тест 2")
empFunc(intArray: ints, stringArray: strings)
ints.append(42)
ints.append(42)
print("Тест 3")
empFunc(intArray: ints, stringArray: strings)